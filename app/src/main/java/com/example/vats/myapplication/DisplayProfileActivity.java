package com.example.vats.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.vats.myapplication.R;

public class DisplayProfileActivity extends Activity {

    private TextView textViewName;
    private TextView textViewMessg;
    private String enteredname;
    private String enteredmssg;

    private static final String TAG = MyLocateProvider.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_profile);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        enteredname = sharedPreferences.getString("namekey", " ");
        enteredmssg = sharedPreferences.getString("messgkey", " ");

        Log.d(TAG, "retrieved values " + enteredname + "," + enteredmssg);

        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewMessg = (TextView) findViewById(R.id.textViewMessg);

        textViewName.setText(enteredname);
        textViewMessg.setText(enteredmssg);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.display_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
