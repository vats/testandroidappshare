package com.example.vats.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;


public class MyLocateProvider extends Activity implements LocationListener {

    private TextView longitudeField;
    private TextView latitudeField;
    private LocationManager locationManager;
    private String provider;

    private TextView username;
    private TextView usermessg;
    private String name;
    private String messg;

    private static final String TAG = MyLocateProvider.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;

    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_locate_provider);
        latitudeField = (TextView) findViewById(R.id.TextView02);
        longitudeField = (TextView) findViewById(R.id.TextView04);

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Criteria to select the location service

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        //Initializing the location fields

        if (location != null){
            System.out.println("Provider"+ provider+ "has been selected");
            onLocationChanged(location);
        }else{
            latitudeField.setText("Location not set");
            longitudeField.setText("Location not set");
        }


    }

    /* request location updates when started */
    @Override
    protected void onResume(){
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }
    /* remove location listener updates on pause */
    @Override
    protected void onPause(){
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location){
        int a = (int) location.getLatitude();
        int b = (int) location.getLongitude();
        latitudeField.setText(String.valueOf(a));
        longitudeField.setText(String.valueOf(b));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras){

    }

    @Override
    public void onProviderEnabled(String provider){
        Toast.makeText(this, "Enabled new provider" + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider){
        Toast.makeText(this, "Disabled provider" + provider, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_locate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // Start a new activity
    public void sendMessage(View view){
        Intent intent = new Intent(this, DisplayMapActivity.class);
        startActivity(intent);
    }

    //on click save
    public void saveData(View view){

        Log.d(TAG, "Called savedata func");

        //get user form data
        username = (TextView) findViewById(R.id.userName);
        usermessg = (TextView) findViewById(R.id.userMessg);

        name = username.getText().toString();
        messg = usermessg.getText().toString();

        Log.d(TAG, "name:"+name+"; messg:"+messg);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("namekey", name);
        editor.putString("messgkey", messg);
        editor.commit();

        Log.d(TAG, "committed write operation on " + name + "," + messg);

        Intent intent = new Intent(this, DisplayProfileActivity.class);

        startActivity(intent);
    }

    // click to view data
    public void viewData(View view){
        Intent newIntent = new Intent(this, DisplayProfileActivity.class);
        startActivity(newIntent);
    }

    public void sendRequest(View newview) throws IOException, ExecutionException, InterruptedException {
        openUrlasynctask task = new openUrlasynctask();
        task.execute(new String[] {"https://api.nutritionix.com/v1_1/search/cheddar%20cheese?fields=item_name%2Citem_id%2Cbrand_name%2Cnf_calories%2Cnf_total_fat&appId=8cf37c0a&appKey=cafb742093a74d66f5ec9ba8c27d0005"});
        String result = task.get();
        Log.d(TAG, "Response returned from async task"+result);
    }

    private class openUrlasynctask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... urls) {
            String response = "";
            for(String url : urls){
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                try{
                    HttpResponse execute = client.execute(httpget);
                    InputStream content = execute.getEntity().getContent();

                    BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
                    String s ="";
                    while((s = buffer.readLine()) != null){
                        response +=s;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            Log.d(TAG, "response:"+response);
            return response;
        }
    }

}
